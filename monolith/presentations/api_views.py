import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation
from events.models import Conference
from django.views.decorators.http import require_http_methods

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


class ListPresentationsEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {
            "status": {
                "id": o.status.id,
                "name": o.status.name,
            }
        }

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):

    presentations = Presentation.objects.filter(conference=conference_id)

    if request.method == "GET":
        return JsonResponse({"presentations": presentations}, encoder=ListPresentationsEncoder, safe=False)
    else:
        content = json.loads(request.body)
    try:
        conference = Conference.objects.get(id=content["conference"])
        content["conference"] = conference
    except Presentation.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid presentation id"},
            status=400,
        )

    presentations = Presentation.create(**content)
    return JsonResponse(
        presentations,
        encoder=ListPresentationsEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    if request.method == "GET":
        return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "presentation" in content:
                presentation = Presentation.objects.get(id=content["presentation"])
                content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation id"},
                status=400
            )
        presentation = Presentation.objects.filter(id=id).update(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False
        )
