import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
def get_photo(city, state):
    url = 'https://api.pexels.com/v1/search'
    headers = {'Authorization': PEXELS_API_KEY }
    params = {'query': f'{city} {state}', 'per_page': 1}
    response = requests.get(url, headers=headers, params=params)
    picture = json.loads(response.content)
    photo_url = picture["photos"][0]["src"]["original"]
    return photo_url


def get_weather_data(city, state):
    response = requests.get(f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}")
    geocoding = response.json()
    if not geocoding or "lat" not in geocoding[0] or "lon" not in geocoding[0]:
        return {"error": "Invalid city/state or geocoding data not available"}
    lon = geocoding[0]["lon"]
    lat = geocoding[0]["lat"]
    weather_response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}")
    weather_data = weather_response.json()
    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    weather = {
        "temperature": temp,
        "description": description
    }
    return weather
